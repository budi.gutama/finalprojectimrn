import React, { Component } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

import Index from './src/screen/index';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showRealApp: false,
    };
  }
  _onDone = () => {
    this.setState({ showRealApp: true });
  };
  _onSkip = () => {
    this.setState({ showRealApp: true });
  };
  _renderItem = ({ item }) => {
    return (
      <View style={{
        flex: 1,
        backgroundColor: item.backgroundColor,
        alignItems: 'center',
        justifyContent: 'space-around',
        paddingBottom: 100
      }}>
        <Image style={item.imageStyle} source={item.image} />
      </View>
    )
};

  render() {
    if (this.state.showRealApp) {
      return (
        <Index />
      );
    } else {
      return (
        <AppIntroSlider
          data={slides}
          renderItem={this._renderItem}
          onDone={this._onDone}
          showSkipButton={true}
          onSkip={this._onSkip}
        />
      );
    }
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  title: {
    fontSize: 26,
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 20,
  },
  text: {
    color: '#fff',
    fontSize: 20,
  },
  image: {
    width: 300,
    height: 50,
    resizeMode: 'contain'
  }
});

const slides = [
  {
    key: 'k1',
    title: '',
    text: '',
    image: require('./assets/pp.jpg'),
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: {width:400, height:300},
    backgroundColor: 'black',
  },
  {
    key: 'k2',
    title: '',
    text: '',
    image: require('./assets/Datang.png'),
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: 'red',
  },
  {
    key: 'k3',
    title: '',
    text: '',
    image: require('./assets/Ngesang.png'),
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: '#2979FF',
  },
  {
    key: 'k4',
    title: '',
    text: '',
    image: require('./assets/Senang.png'),
    titleStyle: styles.title,
    textStyle: styles.text,
    imageStyle: styles.image,
    backgroundColor: 'green',
  },
];