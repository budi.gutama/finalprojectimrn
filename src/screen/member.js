import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { withSafeAreaInsets } from 'react-native-safe-area-context';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data:[],
      searchText: '',
      totalPrice: 0,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://randomuser.me/api?results=20");
    const json = await response.json();
    this.setState({ data: json.results });
  };


  viewDetil(price) {
    this.setState({ totalPrice: this.state.totalPrice + parseFloat(price)})
  }

  render() {
    console.log(this.props.route);
    //const {userName} = this.props.route.params;
    //console.log(userName);
    return (
      <View 
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000',
        }}>
        <LinearGradient
          colors={['#E60D0D', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0, 
            height: 750
          }}
        />
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.headerText}>API randomuser</Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari member..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        
      {/* 
        // Lanjutkan di bawah ini!
        */}
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return <ListItem viewDetil={this.viewDetil.bind(this)} data={item} />
          }}
          numColumns={1}
        />

      </View>
    )
  }
};

class ListItem extends React.Component {

  render() {
    const {viewDetil, data} = this.props
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.picture.large }} style={styles.itemImage} resizeMode='contain' />
        <View style={{marginLeft:10}}>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{`${data.name.first} ${data.name.last}`}</Text>
          <Text style={styles.itemStock}>{`${data.location.street.name} ${data.location.city}`}</Text>
          <Text style={styles.itemPrice}>{`${data.email}`}</Text>
          <Text style={styles.itemStock}>{`${data.location.country}`}</Text>
        </View>
        {/* <Button title='BELI' color='blue' onPress={()=>viewDetil(data.harga)} /> */}
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.92,
    borderRadius:4,
    flex:1,
    padding:2,
    margin:4,
    backgroundColor:'#C4C4C4',
    margin:10,
    flexDirection: 'row',
    alignItems: 'center',
    alignContent:"stretch"
},
  itemImage: {
    height:140,
    width:140
  },
  itemName: {
    fontSize:15,
    fontWeight:'bold'
  },
  itemPrice: {
    fontSize:12,
    color:'blue'
  },
  itemStock: {
  }
})
