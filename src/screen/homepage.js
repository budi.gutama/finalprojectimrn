import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { withSafeAreaInsets } from 'react-native-safe-area-context';

const DEVICE = Dimensions.get('window')

export default class homepage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data:[],
      searchText: '',
      totalPrice: 0,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://randomuser.me/api?results=20");
    const json = await response.json();
    this.setState({ data: json.results });
  };


  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    this.setState({ totalPrice: this.state.totalPrice + parseFloat(price)})
  }

  render() {
    console.log(this.props.route);
    const {name} = this.props.route.params;
    //console.log(userName);
    return (
      <View 
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000',
        }}>
        <LinearGradient
          colors={['#E60D0D', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0, 
            height: 750
          }}
        />
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hallo,{'\n'}
              <Text style={styles.headerText}>{name}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return <ListItem updatePrice={this.updatePrice.bind(this)} data={item} />
          }}
          numColumns={2}
        />

      </View>
    )
  }
};

class ListItem extends React.Component {
  render() {
    const {updatePrice, data} = this.props
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.picture.medium }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.name.first}</Text>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    padding:4,
    margin:8,
    backgroundColor:'white',
    alignItems:'center'
  },
  itemImage: {
    width:140,
    height:140
  },
  itemName: {
    fontSize:14,
    fontWeight:'bold'
  },
  itemPrice: {
    fontSize:12,
    color:'blue'
  },
  itemStock: {
  },
  itemButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2980b9',
    width:60,
    height:35,
  },
  buttonText: {
    color:'white',
    fontWeight:'bold'
  }
})
