import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default class welcome extends React.Component {
    render(){
        const navigate = this.props.navigation;
        return(
            <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#000000',
            }}>
            <LinearGradient
              colors={['#E60D0D', 'transparent']}
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0, 
                height: 750
              }}
            />
            <Image source={require('../../assets/g1_1.jpg')} style={styles.photoProfil} />
            <Text style={styles.profileName}>G E M I L A N G</Text>
            <Text style={styles.cityName}>BANDUNG</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
  photoProfil:{
      width: 220,
      height: 220,
      position: 'absolute',
      borderRadius:180,
      top:30,
      alignItems:"center" 
  },
  profileName:{
      fontSize:30,
      alignItems:"center",
      position:"absolute",
      top:260
  },
  cityName:{
      fontSize:18,
      alignItems:"center",
      position:"absolute",
      top:300,
      color:'white'
  }
});  