import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Image, TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
import { LinearGradient } from 'expo-linear-gradient';
import styles from '../style'

export default class login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
    }
  }

  loginHandler() {
    console.log(this.state.userName, ' ', this.state.password);
    const navigate = this.props.navigation;
    navigate.navigate("homepage", {
      screen : 'member',
      params : { name: "REGISTER" }
    });
  }

  render() {
    const navigate = this.props.navigation;
    return(
        <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000',
        }}>
        <LinearGradient
          colors={['#E60D0D', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0, 
            height: 750
          }}
        />
        <Image source={require('../../assets/sixam.png')} style={styles.appName} />
        <View style={styles.formContainer}>
        <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Username</Text>
              <TextInput
                style={styles.textInput}
                placeholder='email atau username'
                onChangeText={userName => this.setState({ userName })}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Password</Text>
              <TextInput
                style={styles.textInput}
                placeholder='Masukkan Password'
                onChangeText={password => this.setState({ password })}
                secureTextEntry={true}
              />
            </View>
          </View>

          <View style={styles.inputContainer}>
            <View>
              <Text style={styles.labelText}>Nama Pengguna</Text>
              <TextInput
                style={styles.textInput}
                placeholder='nama'
                onChangeText={nama_lengkap => this.setState({ nama_lengkap })}
              />
            </View>
          </View>

          <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Password Salah</Text>
            <TouchableOpacity  style={styles.ButtonItem} onPress={() => this.loginHandler()}>
                <Text style={styles.ButtonText}>Register</Text>
            </TouchableOpacity>
        </View>
      </View>
    )
  }
};


