import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import { withSafeAreaInsets } from 'react-native-safe-area-context';

const DEVICE = Dimensions.get('window')

export default class event extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data:[],
      searchText: '',
      totalPrice: 0,
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    const response = await fetch("https://api.kawalcorona.com/indonesia/provinsi");
    const json = await response.json();
    this.setState({ data: json });
  };


  viewDetil(price) {
    this.setState({ totalPrice: this.state.totalPrice + parseFloat(price)})
  }

  render() {
    //console.log(this.state.data);
    console.log(this.props.route);
    //const {userName} = this.props.route.params;
    //console.log(userName);
    return (
      <View 
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: '#000000',
        }}>
        <LinearGradient
          colors={['#E60D0D', 'transparent']}
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0, 
            height: 750
          }}
        />
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Text style={styles.headerText}>API Kawal Corona</Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari member..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        
        <FlatList
          data={this.state.data}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item }) => {
            return <ListItem viewDetil={this.viewDetil.bind(this)} data={item} />
          }}
          numColumns={2}
        />

      </View>
    )
  }
};

class ListItem extends React.Component {

  render() {
    const {viewDetil, data} = this.props
    return (
      <View style={styles.itemContainer}>
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.attributes.Provinsi}</Text>
        <Text style={styles.itemStock}>Positif :{data.attributes.Kasus_Posi}</Text>
        <Text style={styles.itemStock}>Sembuh :{data.attributes.Kasus_Semb}</Text>
        <Text style={styles.itemStock}>menggal :{data.attributes.Kasus_Meni}</Text>
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
    borderRadius:4,
    padding:2,
    margin:4,
    backgroundColor:'#C4C4C4',
    margin:10,
    alignContent:"stretch"
},
  itemImage: {
    height:140,
    width:140
  },
  itemName: {
    fontSize:15,
    fontWeight:'bold'
  },
  itemPrice: {
    fontSize:12,
    color:'blue'
  },
  itemStock: {
  }
})
