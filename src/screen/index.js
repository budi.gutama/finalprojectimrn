import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";

import welcomeScreen from './welcome';
import loginScreen from './login';
import registerScreen from './register';
import homeScreen from './homepage';
import memberScreen from './member';
import eventScreen from './event';
import aboutScreen from './about';

const Stack = createStackNavigator();

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();

const HomeStackScreen = () => (
  <HomeStack.Navigator>
    <HomeStack.Screen name="homepage" component={homeScreen}  options={{ headerShown: false }} />
    <HomeStack.Screen
      name="member"
      component={memberScreen}
      options={({ route }) => ({
        title: route.params.name
      })}
    />
  </HomeStack.Navigator>
);

const SearchStackScreen = () => (
  <SearchStack.Navigator>
    <SearchStack.Screen name="Search" component={Search} />
    <SearchStack.Screen name="Search2" component={Search2} />
  </SearchStack.Navigator>
);

const ProfileStack = createStackNavigator();
const ProfileStackScreen = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>
);

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="homepage" component={homeScreen}  options={{ headerShown: false }} />
    <Tabs.Screen name="member" component={memberScreen} options={{ headerTitle: 'Member' }} />
    <Tabs.Screen name="event" component={eventScreen} options={{ headerTitle: 'Event' }} />
    <Tabs.Screen name="about" component={aboutScreen} options={{ headerTitle: 'About Me' }} />
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

export default () => (
  <NavigationContainer>
    <Stack.Navigator initialRouteName="welcome" >
      <Stack.Screen name='welcome' component={welcomeScreen} options={{ headerShown: false }} />
      <Stack.Screen name='login' component={loginScreen} />
      <Stack.Screen name='register' component={registerScreen} />
      <Stack.Screen name='homepage' component={TabsScreen}/>
    </Stack.Navigator>
  </NavigationContainer>
);


