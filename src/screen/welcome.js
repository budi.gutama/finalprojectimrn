import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default class welcome extends React.Component {
    render(){
        const navigate = this.props.navigation;
        return(
            <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: '#000000',
            }}>
            <LinearGradient
              colors={['#E60D0D', 'transparent']}
              style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0, 
                height: 750
              }}
            />
                <Image source={require('../../assets/sixam.png')} style={styles.appName} />
 
                <View style={styles.buttonContainer}>
                <TouchableOpacity style={styles.ButtonItem} onPress={() => {navigate.navigate("login")}}>
                    <Text style={styles.ButtonText}> Log In</Text>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.ButtonItem} onPress={() => {navigate.navigate("register")}}>
                    <Text style={styles.ButtonText}> Register</Text>
                </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    appName:{
      width: 330,
      height: 55,
      position: 'absolute',
      top:200,
      alignItems:"center" 
    },
    buttonContainer:{
        elevation: 2,
        margin:10,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom:20,
        justifyContent: 'space-around'  
    },
    ButtonItem:{
        height: 45,
        flex: 1,
        marginLeft:10,
        borderColor: "#0000",
        backgroundColor:"#fff",
        borderWidth: 1,
        borderRadius: 5,
        alignItems:'center',
        justifyContent: 'space-around'  
    },
    ButtonText:{
        fontSize:18
      }
});  