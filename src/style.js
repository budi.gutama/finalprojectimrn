import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    appName:{
      width: 300,
      height: 50,
      marginBottom:100,
      alignItems:"center" 
    },
    buttonContainer:{
        elevation: 2,
        margin:10,
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        bottom:20,
        justifyContent: 'space-around'  
    },
    ButtonItem:{
        height: 46,
        marginLeft:10,
        borderColor: "#0000",
        backgroundColor:"#1BA6F4",
        borderWidth: 1,
        borderRadius: 5,
        alignItems:'center',
        justifyContent: 'space-around'  
    },
    ButtonText:{
        fontSize:18,
        color:'white'
    },
    container: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
    },
    formContainer: {
        justifyContent: 'center'
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center',
        marginBottom: 16
    },
    labelText: {
        fontWeight: 'bold',
        marginBottom:5,
        color:'white'
    },
    textInput: {
        width: 300,
        height:35,
        borderRadius:4,
        backgroundColor: 'white',
        padding:5
    },
    errorText: {
        color: 'white',
        fontSize:18,
        textAlign: 'center',
        marginBottom: 16,
    },
    hiddenErrorText: {
        color: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
    }
});